﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuoteAppTask.Models
{
    public class QuoteRepository
    {
        private static List<QuoteViewModel> quotes = new List<QuoteViewModel>();
        public static IEnumerable<QuoteViewModel> Quotes => Quotes;
        public static void AddQuotes(QuoteViewModel quoteViewModel)
        {
            quotes.Add(quoteViewModel);
        }
    }
}
