﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuoteAppTask.Models
{
    public class QuoteViewModel 
    {
        //public int Id { get; set; }
        public string Quote { get; set; }
        public string Author { get; set; }
    }
}
